#!/usr/bin/env node

import {
    dirname,
    join as joinPath
} from 'node:path';
import {
    fileURLToPath
} from 'node:url';
import {
    mkdir,
    writeFile
} from 'node:fs/promises';

import {
    program
} from 'commander';
import {
    JSDOM
} from 'jsdom';
import axios from 'axios';
import cliProgress from 'cli-progress';

const
    getPages = sitemapDocument =>
        [...sitemapDocument.querySelectorAll('url > loc')]
            .map(pageElement => pageElement.textContent),
    __dirname = dirname(fileURLToPath(import.meta.url));

program
    .description('Export MediaWiki from sitemap to HTML')
    .argument('<sitemap>', 'Sitemap URL')
    .option('--extension <extension>', 'Default file extension')
    .action(async (
        rootSitemapUrl,
        {
            extension: defaultFileExtension = 'html'
        }
    ) => {
        const
            startTimestamp = Date.now(),
            {
                hostname: rootSitemapUrlHostname,
                pathname: rootSitemapUrlPathname
            } = new URL(rootSitemapUrl),
            rootSitemapFilename = rootSitemapUrlPathname.split('/').slice(-1)[0],
            rootPath = joinPath(
                __dirname,
                'data',
                rootSitemapUrlHostname,
                rootSitemapFilename
            ),
            sitemaps = [];
        const rootSitemapDocument = new JSDOM((await axios(rootSitemapUrl)).data).window.document;
        {
            const childSitemapElements = rootSitemapDocument.querySelectorAll('sitemap');
            if(childSitemapElements.length){
                sitemaps.push(...[...childSitemapElements].map(sitemapElement => {
                    const url = sitemapElement.querySelector('sitemap > loc').textContent;
                    return {
                        url,
                        filename: new URL(url).pathname.split('/').slice(-1)[0]
                    }
                }));
            }
            else
                sitemaps.push({ filename: rootSitemapFilename, pageUrls: getPages(rootSitemapDocument) });
        }
        const
            multibar = new cliProgress.MultiBar(
                {
                    clearOnComplete: true,
                    format: '{bar} | {index}/{count} | {filename}'
                },
                cliProgress.Presets.shades_classic
            ),
            sitemapBar = multibar.create(sitemaps.length);
        let totalPageCount = 0;
        for(let sitemapIndex = 0; sitemapIndex < sitemaps.length; sitemapIndex++){
            const sitemap = sitemaps[sitemapIndex];
            sitemapBar.update(
                0,
                {
                    index: String(sitemapIndex).padStart(sitemaps.length.toString().length, '0'),
                    count: sitemaps.length,
                    filename: sitemap.filename
                }
            );
            if(!sitemap.pageUrls){
                const sitemapDocument = new JSDOM((await axios(sitemap.url)).data).window.document;
                sitemap.pageUrls = getPages(sitemapDocument);
            }
            const pageBar = multibar.create(sitemap.pageUrls.length);
            for(let pageUrlIndex = 0; pageUrlIndex < sitemap.pageUrls.length; pageUrlIndex++){
                const pageUrl = sitemap.pageUrls[pageUrlIndex];
                const
                    {
                        pathname: pageUrlPathname
                    } = new URL(pageUrl),
                    pageFilename = `${pageUrlPathname.split('/').slice(-1)[0]}.${defaultFileExtension}`;
                pageBar.update(
                    pageUrlIndex,
                    {
                        index: String(pageUrlIndex).padStart(sitemap.pageUrls.length.toString().length, '0'),
                        count: sitemap.pageUrls.length,
                        filename: pageFilename
                    }
                );
                let pagePrintableUrl = new URL(pageUrl);
                pagePrintableUrl.searchParams.set('printable', 'yes');
                pagePrintableUrl = pagePrintableUrl.toString();
                const
                    pageContainerElement = new JSDOM((await axios(pagePrintableUrl)).data)
                        .window.document.querySelector('#mw-content-text'),
                    pagePath = joinPath(rootPath, ...pageUrlPathname.split('/').slice(0, -1));
                pageContainerElement
                    .querySelectorAll('a[href^="/"]')
                    .forEach(anchorElement =>
                        anchorElement.setAttribute(
                            'href',
                            `${anchorElement.getAttribute('href')}.${defaultFileExtension}`
                        )
                    );
                pageContainerElement
                    .querySelectorAll(`
                        a[href^="https://${rootSitemapUrlHostname}"],
                        a[href^="http://${rootSitemapUrlHostname}"]
                    `)
                    .forEach(anchorElement =>
                        anchorElement.setAttribute(
                            'href',
                            `${new URL(anchorElement.getAttribute('href')).pathname}.${defaultFileExtension}`
                        )
                    );
                const pageContent = pageContainerElement.innerHTML;
                await mkdir(pagePath, { recursive: true });
                await writeFile(joinPath(pagePath, pageFilename), pageContent, 'utf8');
                totalPageCount++;
            }
            multibar.remove(pageBar);
        }
        multibar.stop();
        const endTimestamp = Date.now();
        console.log(`Exported ${totalPageCount} pages in ${Math.round((endTimestamp - startTimestamp) / 1000)}s at ${rootPath}`);
    });

program.parse();